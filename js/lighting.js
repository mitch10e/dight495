/**
 * Created by mitch10e on 9/21/15.
 * Using http://threejs.org/
 */

var lights = [];
var isShowingLights = false;

var COLOR = {
    WHITE       : 0xFFFFFF,
    DIMWHITE    : 0xAAAAAA,
    SOFTWHITE   : 0x404040,
    RED         : 0xFF0000,
    GREEN       : 0x00FF00,
    BLUE        : 0x0000FF
};

// Show lights in scene
function toggleShowLights() {
    $('#showLightsDiv').toggleClass('greenHover');
    isShowingLights = !isShowingLights;
    for(var i = 0; i < lights.length; i++) {
        lights[i].shadowCameraVisible = isShowingLights;
    }
}

function toggleEditLights() {
    $('#lightControls').toggleClass('hidden');
    $('#editLightsDiv').toggleClass('greenHover');
}

// Create new ambient Three.js light
function addAmbientLight(color) {
    color = typeof color !== 'undefined' ? color : COLOR.DIMWHITE;
    var light = new THREE.AmbientLight(color);
    lights.push(light);
    scene.add(light);
}

// Create new point Three.js light
function addPointLight(x, y, z, color, intensity) {
    color = typeof color !== 'undefined' ? color : COLOR.WHITE;
    intensity = typeof intensity !== 'undefined'? intensity : 1;
    var light = new THREE.PointLight(color, intensity);
    light.position.set(x, y ,z);
    light.shadowCameraVisible = isShowingLights;
    lights.push(light);
    scene.add(light);
}


// This is the light used in the project - Directional light
function addDirectionalLight( x, y, z, color, intensity, magnitude) {
    color = typeof color !== 'undefined' ? color : COLOR.WHITE;
    intensity = typeof intensity !== 'undefined'? intensity : 1;
    magnitude = typeof  magnitude !== 'undefined' ? magnitude : 5;

    var light = new THREE.DirectionalLight( color, intensity );
    light.position.set( x, y, z );
    light.shadowCameraVisible = isShowingLights;
    light.castShadow = true;
    //light.onlyShadow = true;

    light.shadowCameraLeft = -magnitude;
    light.shadowCameraRight = magnitude;
    light.shadowCameraTop = magnitude;
    light.shadowCameraBottom = -magnitude;

    light.shadowCameraNear = 1;
    light.shadowCameraFar = 1000;

    light.shadowMapWidth = 1000;
    light.shadowMapHeight = 1000;

    light.shadowBias = -0.001;
    light.shadowDarkness = 0.15;

    lights.push(light);
    scene.add(light);
}

// Display light in scene on per light basis
function showLight(light) {
    lights[light].shadowCameraVisible = true;
}

function hideLight(light) {
    lights[light].shadowCameraVisible = false;
}

// Used to get a specific light in the scene
function getCurrentLightSelected() {
    var select = $('#lights-select');
    var index = select[0].selectedIndex;
    return lights[index];
}

function updateLightPositionX(input) {
    var light = getCurrentLightSelected();
    light.position.x = parseFloat(input.value);
    updateLightInfo();
}

function updateLightPositionY(input){
    var light = getCurrentLightSelected();
    light.position.y = parseFloat(input.value);
}

function updateLightPositionZ(input) {
    var light = getCurrentLightSelected();
    light.position.z = parseFloat(input.value);
    updateLightInfo();
}

function updateLightColor(input) {
    var light = getCurrentLightSelected();
    var color = input.value;
    light.color.set(color);
    updateLightInfo();
}

function loadLightData() {
    var light = getCurrentLightSelected();
    if (light.type != "DirectionalLight") {
        $("#xLight").prop("disabled", true);
        $("#yLight").prop("disabled", true);
        $("#zLight").prop("disabled", true);
    } else {
        $("#xLight").prop("disabled", false);
        $("#yLight").prop("disabled", false);
        $("#zLight").prop("disabled", false);
    }
    var r = light.color.r * 255;
    var g = light.color.g * 255;
    var b = light.color.b * 255;
    var hex = rgbToHex(r, g, b);
    $("#xLight").val(light.position.x);
    $("#yLight").val(light.position.y);
    $("#zLight").val(light.position.z);
    $("#colorLight").val(hex);
}

function createLightOptions() {
    var select = $('#lights-select');
    var index = select[0].selectedIndex;
    select.html("");
    $.each(lights, function(idx, value) {
        select.append("<option value='" + value.type + "'> " + value.type + " </option>");
    });
    console.log(index);
    if(index > -1) {
        select.find('option').eq(index).prop('selected', true);
    }
    loadLightData();
}


function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function showCurrentLight() {
    var select = $('#lights-select');
    var index = select[0].selectedIndex;
    showLight(index);
    updateLightInfo();
}

function hideCurrentLight() {
    var select = $('#lights-select');
    var index = select[0].selectedIndex;
    hideLight(index);
    updateLightInfo();
}

// used to add lights from the UI
function createNewLight() {
    addDirectionalLight(1, 1, 1);
    updateLightInfo();
}

// Cannot remove the ambient light
function removeCurrentLight() {
    light = getCurrentLightSelected();
    var select = $('#lights-select');
    var index = select[0].selectedIndex;
    console.log(index);
    // No killing the ambient light!
    if(lights[index].type != "AmbientLight") {
        lights.splice(index, 1);
        select[0].selectedIndex = 0;
        scene.remove(light);
    }
    updateLightInfo();
}