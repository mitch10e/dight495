/**
 * Created by mitch10e on 9/21/15.
 */

var controlMenuOpen = false;

var objectMenuOpen = false;
var cameraMenuOpen = false;
var lightingMenuOpen = false;

var header = $('header');
var controlMenu = $('#controlOptions');

var objectMenu = $('#objectOptions');
var cameraMenu = $('#cameraOptions');
var lightingMenu = $('#lightingOptions');



$("#controlMenuButton").click(function() {
    toggleControlMenu();
    $('#controlMenuDiv').toggleClass('orangeHover');
});

$("#objectMenuButton")
    .click(function() {
        toggleObjectMenu();
        $('#objectMenuDiv').toggleClass('redHover');
    })
    .hover(function() {
        $("#controlOptions")
            .removeClass('orangeBar')
            .removeClass('blueBar')
            .removeClass('greenBar')
            .addClass('redBar');
    }
);

$("#cameraMenuButton")
    .click(function() {
        toggleCameraMenu();
        $('#cameraMenuDiv').toggleClass('blueHover');
    })
    .hover(function() {
        $("#controlOptions")
            .removeClass('orangeBar')
            .removeClass('redBar')
            .removeClass('greenBar')
            .addClass('blueBar');
    }
);

$("#lightingMenuButton")
    .click(function() {
        toggleLightingMenu();
        $('#lightingMenuDiv').toggleClass('greenHover');
    })
    .hover(function() {
        $("#controlOptions")
            .removeClass('orangeBar')
            .removeClass('redBar')
            .removeClass('blueBar')
            .addClass('greenBar');
    }
);

$('#scaleButton').click(function() {
    toggleScaleSlider();
});

$('#cameraInfoButton').click(function() {
    toggleCameraInfo();
});

$('#objectInfoButton').click(function() {
    toggleObjectInfo();
});

$('#cameraControlsButton').click(function() {
    toggleCameraControls();
});

$('#cameraResetButton').click(function() {
    resetCameraPosition();
    updateCameraInfo();
});

$('#rotateXButton').click(function() {
    toggleRotateObjectX();
});

$('#rotateYButton').click(function() {
    toggleRotateObjectY();
});

$('#rotateZButton').click(function() {
    toggleRotateObjectZ();
});

$('#loadObjectButton').click(function() {
    toggleLoadObjectModal();
});

$('#lightsInfoButton').click(function() {
    toggleLightsInfo();
});

$('#showLightsButton').click(function() {
    toggleShowLights();
});

$('#editLightsButton').click(function() {
    toggleEditLights();
});


// Opens menus up top, places them in correct location relative to parent.
function openMenu(menu, beneath) {
    var top = 3;
    var dist = parseInt(beneath.css('top'));
    if(!isNaN(dist)) {
        top += dist;
    }
    top += parseInt(beneath.height());
    menu.css('top', top + 'px');
}

// Opens menus up top, places them in correct location relative to parent.
function openSubMenu(menu, beneath) {
    var top = -5;
    var dist = parseInt(beneath.css('top'));
    if(!isNaN(dist)) {
        top += dist;
    }
    top += parseInt(beneath.height());
    menu.css('top', top + 'px');
}

// Move menu out of view
function closeMenu(menu) {
    menu.css('top', '-1em');
}


function toggleControlMenu() {
    if(controlMenuOpen) {
        closeMenu(controlMenu);
        closeObjectMenu();
        closeCameraMenu();
        closeLightingMenu();
    } else {
        openMenu(controlMenu, header)
    }
    controlMenuOpen = !controlMenuOpen;
}

function toggleObjectMenu() {
    if(objectMenuOpen) {
        closeMenu(objectMenu);
    } else {
        openSubMenu(objectMenu, controlMenu);
        closeCameraMenu();
        closeLightingMenu();
    }
    objectMenuOpen = !objectMenuOpen;
}

function toggleCameraMenu() {
    if(cameraMenuOpen) {
        closeMenu(cameraMenu);
    } else {
        openSubMenu(cameraMenu, controlMenu);
        closeObjectMenu();
        closeLightingMenu();
    }
    cameraMenuOpen = !cameraMenuOpen;
}

function toggleLightingMenu() {
    if(lightingMenuOpen) {
        closeMenu(lightingMenu);
    } else {
        openSubMenu(lightingMenu, controlMenu);
        closeObjectMenu();
        closeCameraMenu();
    }
    lightingMenuOpen = !lightingMenuOpen;
}

function closeObjectMenu() {
    closeMenu(objectMenu);
    objectMenuOpen = false;
    $('#objectMenuDiv').removeClass('redHover');
}

function closeCameraMenu() {
    closeMenu(cameraMenu);
    cameraMenuOpen = false;
    $('#cameraMenuDiv').removeClass('blueHover');
}

function closeLightingMenu() {
    closeMenu(lightingMenu);
    lightingMenuOpen = false;
    $('#lightingMenuDiv').removeClass('greenHover');
}

function resetAll() {
    resetObjectOrientation();
    resetCameraPosition();
}

// -- Control Panel

$(function() {
    $('#scaleSlider').slider({
        min: 1,
        max: 100,
        step: 0.01,
        value: scale,
        slide: function( event, ui ) {
            scaleObject(object, (ui.value / 100));
            scale = ui.value * 0.01;
            if(ui.value >= 10) {
                $(this).find('.ui-slider-handle').text(ui.value.toFixed(0));
            } else {
                $(this).find('.ui-slider-handle').text(ui.value.toFixed(1));
            }
        },
        create: function(ui) {
            var v=$(this).slider('value');
            $(this).find('.ui-slider-handle').text(v.toFixed(1));
        }
    });
});

// -- Object Controls

function toggleObjectInfo() {
    updateCameraInfo();
    $('.objectInfo').toggleClass('hidden');
    $('#objectInfoDiv').toggleClass('redHover');
}

function updateObjectInfo() {
    $('#objScale').text("x" + (scale * 100).toFixed(2));
    $('#objX').text("X: " + object.position.x.toFixed(3));
    $('#objY').text("Y: " + object.position.y.toFixed(3));
    $('#objZ').text("Z: " + object.position.z.toFixed(3));
    $('#objXrot').text("X: " + object.rotation.x.toFixed(3));
    $('#objYrot').text("Y: " + object.rotation.y.toFixed(3));
    $('#objZrot').text("Z: " + object.rotation.z.toFixed(3));
}

function toggleScaleSlider() {
    $('#scaleSlider').toggleClass('hidden');
    $('#scaleButtonDiv').toggleClass('redHover');
}

function toggleLoadObjectModal() {
    $('#load_modal').fadeToggle('fast');
    $('#loadObjectDiv').toggleClass('redHover');
}

// -- Camera Controls

function toggleCameraInfo() {
    updateCameraInfo();
    $('.cameraInfo').toggleClass('hidden');
    $('#cameraInfoDiv').toggleClass('blueHover');
}

function updateCameraInfo() {
    $('#camX').text("X: " + camera.position.x.toFixed(3));
    $('#camY').text("Y: " + camera.position.y.toFixed(3));
    $('#camZ').text("Z: " + camera.position.z.toFixed(3));
    $('#camXrot').text("X: " + camera.rotation.x.toFixed(3));
    $('#camYrot').text("Y: " + camera.rotation.y.toFixed(3));
    $('#camZrot').text("Z: " + camera.rotation.z.toFixed(3));
}

function toggleCameraControls() {
    $('#cameraControls').toggleClass('hidden');
    $('#cameraControlsDiv').toggleClass('blueHover');
}

function updateCameraControlDescription(text) {
    $('#camControlDescription').text(text);
}

// -- Lighting

function toggleLightsInfo() {
    updateLightInfo();
    $('.lightInfo').toggleClass('hidden');
    $('#lightsInfoDiv').toggleClass('greenHover');
}

function updateLightInfo() {
    createLightOptions();
    var list = $('#lightsList');
    list.html("");
    for(var i = 0; i < lights.length; i++) {
        var light = lights[i];
        var r = light.color.r * 255;
        var g = light.color.g * 255;
        var b = light.color.b * 255;
        var hex = rgbToHex(r, g, b);
        var span = $('<span />').attr({
            'class' : 'info data'
        }).css({
            'color' : hex
        }).html(
            lights[i].type +
            " X: " + light.position.x.toFixed(2) +
            " Y: " + light.position.y.toFixed(2) +
            " Z: " + light.position.z.toFixed(2)
        );
        var br = $('<br />');
        list.append(span);
        list.append(br);
    }
}

// -- Keyboard

$(document).keydown(function(e) {
    var key = e.keyCode;
    //console.log(key);
    if(key == 27) { // ESC

    }

    if(key == 188) { // ,
        toggleRotateObjectX();
    }

    if(key == 190) { // .
        toggleRotateObjectY();
    }

    if(key == 191) { // /
        toggleRotateObjectZ();
    }

    if(key == 219) { // [
        toggleObjectInfo();
    }

    if(key == 220) { // \
        toggleLightsInfo();
    }

    if(key == 221) { // ]
        toggleCameraInfo();
    }

    if(key == 192) { // ~
        toggleCameraControls();
    }

    if(key == 222) { // '
        toggleShowLights();
    }

    if(key == 186) { // ;
        toggleScaleSlider();
    }

    if(key == 65) { // A
        updateCameraControlDescription("Move Left");
        if(timer == 0) {
            timer = setInterval(moveCameraLeft, tick);
        }
        $('#A').addClass('blueHover');
    }

    if(key == 66) { // B

    }

    if(key == 67) { // C
        updateCameraControlDescription("Rotate Object Z");
        if(timer == 0) {
            timer = setInterval(rotateObjectZ, tick);
        }
        $('#C').addClass('orangeHover');
    }

    if(key == 68) { // D
        updateCameraControlDescription("Move Right");
        if(timer == 0) {
            timer = setInterval(moveCameraRight, tick);
        }
        $('#D').addClass('blueHover');
    }

    if(key == 69) { // E
        updateCameraControlDescription("Turn Right");
        if(timer == 0) {
            timer = setInterval(turnRight, tick);
        }
        $('#E').addClass('orangeHover');
    }

    if(key == 70) { // F
        updateCameraControlDescription("Move Down");
        if(timer == 0) {
            timer = setInterval(moveCameraDown, tick);
        }
        $('#F').addClass('greenHover');
    }

    if(key == 71) { // G

    }

    if(key == 76) { // L
        toggleLoadObjectModal();

    }

    if(key == 81) { // Q
        updateCameraControlDescription("Turn Left");
        if(timer == 0) {
            timer = setInterval(turnLeft, tick);
        }
        $('#Q').addClass('orangeHover');
    }

    if(key == 82) { // R
        updateCameraControlDescription("Move Up");
        if(timer == 0) {
            timer = setInterval(moveCameraUp, tick);
        }
        $('#R').addClass('greenHover');
    }

    if(key == 83) { // S
        updateCameraControlDescription("Move Backward");
        if(timer == 0) {
            timer = setInterval(moveCameraBackward, tick);
        }
        $('#S').addClass('blueHover');
    }

    if(key == 84) { // T

    }

    if(key == 86) { // V
        updateCameraControlDescription("Reset Object Orientation");
        if(timer == 0) {
            timer = setInterval(resetAll, tick);
        }
        $('#V').addClass('grayHover');
    }

    if(key == 87) { // W
        updateCameraControlDescription("Move Forward");
        if(timer == 0) {
            timer = setInterval(moveCameraForward, tick);
        }
        $('#W').addClass('blueHover');
    }

    if(key == 88) { // X
        updateCameraControlDescription("Rotate Object Y");
        if(timer == 0) {
            timer = setInterval(rotateObjectY, tick);
        }
        $('#X').addClass('greenHover');
    }

    if(key == 90) { // Z
        updateCameraControlDescription("Rotate Object X");
        if(timer == 0) {
            timer = setInterval(rotateObjectX, tick);
        }
        $('#Z').addClass('orangeHover');
    }



});

$(document).keyup(function(e) {
    clearInterval(timer);
    timer = 0;
    var key = e.keyCode;
    updateCameraControlDescription("");
    if(key == 65) { // A
        $('#A').removeClass('blueHover');
    }
    if(key == 66) { // B

    }
    if(key == 67) { // C
        $('#C').removeClass('orangeHover');
    }
    if(key == 68) { // D
        $('#D').removeClass('blueHover');
    }
    if(key == 69) { // E
        $('#E').removeClass('orangeHover');
    }
    if(key == 70) { // F
        $('#F').removeClass('greenHover');
    }
    if(key == 71) { // G

    }
    if(key == 76) { // L

    }

    if(key == 81) { // Q
        $('#Q').removeClass('orangeHover');
    }
    if(key == 82) { // R
        $('#R').removeClass('greenHover');
    }
    if(key == 83) { // S
        $('#S').removeClass('blueHover');
    }
    if(key == 84) { // T

    }
    if(key == 86) { // V
        $('#V').removeClass('grayHover');
    }
    if(key == 87) { // W
        $('#W').removeClass('blueHover');
    }
    if(key == 88) { // X
        $('#X').removeClass('greenHover');
    }
    if(key == 90) { // Z
        $('#Z').removeClass('orangeHover');
    }
});


var timer = 0;
var tick = 10;
// Display tooltip in camera controls box, also, reapeat function per tick
$('#Q').hover(function() {
    updateCameraControlDescription("Turn Left");
}).mousedown(function() {
    timer = setInterval(turnLeft, tick);
});

$('#W').hover(function() {
    updateCameraControlDescription("Move Forward");
}).mousedown(function() {
    timer = setInterval(moveCameraForward, tick);
});

$('#E').hover(function() {
    updateCameraControlDescription("Turn Right");
}).mousedown(function() {
    timer = setInterval(turnRight, tick);
});

$('#R').hover(function() {
    updateCameraControlDescription("Move Up");
}).mousedown(function() {
    timer = setInterval(moveCameraUp, tick);
});

$('#A').hover(function() {
    updateCameraControlDescription("Move Left");
}).mousedown(function() {
    timer = setInterval(moveCameraLeft, tick);
});

$('#S').hover(function() {
    updateCameraControlDescription("Move Backward");
}).mousedown(function() {
    timer = setInterval(moveCameraBackward, tick);
});

$('#D').hover(function() {
    updateCameraControlDescription("Move Right");
}).mousedown(function() {
    timer = setInterval(moveCameraRight, tick);
});

$('#F').hover(function() {
    updateCameraControlDescription("Move Down");
}).mousedown(function() {
    timer = setInterval(moveCameraDown, tick);
});

$('#Z').hover(function() {
    updateCameraControlDescription("Rotate Object X");
}).mousedown(function() {
    timer = setInterval(rotateObjectZ, tick);
});

$('#X').hover(function() {
    updateCameraControlDescription("Rotate Object Y");
}).mousedown(function() {
    timer = setInterval(rotateObjectX, tick);
});

$('#C').hover(function() {
    updateCameraControlDescription("Rotate Object Z");
}).mousedown(function() {
    timer = setInterval(rotateObjectY, tick);
});

$('#V').hover(function() {
    updateCameraControlDescription("Reset All");
}).mousedown(function() {
    resetAll();
});

$('.camControl').on("mouseleave", function() {
    updateCameraControlDescription("");
}).mouseup(function() {
    clearInterval(timer);
    timer = 0;
});


// Drop ply file into box
function handleFileDrop(event) {
    event.stopPropagation();
    event.preventDefault();
    $("#progressbar").toggleClass('hidden');
    var file = event.dataTransfer.files[0]; // FileList object.
    // files is a FileList of File objects. List some properties.
    var output = [];
    output.push((file.name));
    document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + output.join('') + '</div>';
    updateProgressBar(10);
    setTimeout(function() {
        readSingleFile(file);
    }, 1000);
}

function handleFileDragOver(event) {
    event.stopPropagation();
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';

    $("#drop_zone").addClass('greenDotBorder');
}

function handleFileDragOut(event) {
    event.stopPropagation();
    event.preventDefault();
    $("#drop_zone").removeClass('greenDotBorder');
}

var dropZone = document.getElementById('drop_zone');
dropZone.addEventListener('dragenter', handleFileDragOver, false);
dropZone.addEventListener('dragover', handleFileDragOver, false);
dropZone.addEventListener('dragleave', handleFileDragOut, false);
dropZone.addEventListener('drop', handleFileDrop, false);

function clampInput(input, min, max) {
    if (input.value < min) input.value = min;
    if (input.value > max) input.value = max;
}

// Used to keep track of where parsing a new PLY file is.
function updateProgressBar(amount) {

    var progress = $("#progress");
    var current = progress.attr("value");

    if(current > amount) {
        return;
    } else {

        for(var i = current; i < amount + 1; i++) {
            progress.animate({ value: "+=1" }, 0.001);
        }
        progress.animate({ value: amount }, 0);
    }

    var red = '#ff3344 !important';
    var orange = '#ffaf3a !important';
    var blue = '#66AAFF !important';
    var green = '#21972e !important';

    if (0 <= amount && amount <= 24) {
        document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "loading file" +'</div>';
    } else if (25 <= amount && amount <= 49) {
        document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "parsing file" +'</div>';
    } else if (50 <= amount && amount <= 74) {
        document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "loading scene" +'</div>';
    } else if (75 <= amount && amount <= 100) {
        document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "rendering" +'</div>';
    } else {
        document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "rendering" +'</div>';
    }

    if(progress.attr("value") > 100) {
        resetProgressBar();
    }
}

// Need to reset after each PLY file uploaded.
function resetProgressBar() {
    var progress = $("#progress");
    progress.animate({value: 0}, 0);
    document.getElementById('drop_zone').innerHTML = '<div class="fileName">' + "Drop .PLY File Here" + '</div>';

}

// On Page Load...
$(function() {
    init();
    updateCameraInfo();
    updateLightInfo();
    toggleLoadObjectModal();
});